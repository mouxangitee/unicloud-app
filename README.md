# unicloud-app

#### 介绍
对接unicloud-router-iview-admin的移动端app

# 安装步骤

推荐使用yarn来替换npm，具体使用安装方法自行百度

## 方法一：
* 使用[uniapp插件市场](https://ext.dcloud.net.cn/plugin?id=1639)一键工程导入
* 选择项目，右键->使用命令行窗口打开所在目录，或使用系统自带的CMD进入项目运行 yarn(npm) run build 进行下载依赖包
* 创建或选择云服务空间,进入manifest.json可视化重新获取appid
* 右键上传cloudfunctions-aliyun文件夹下所有云函数(云函数上传完后请进云开发控制台将函数执行内存改至最大)
* 选择/cloudfunctions-aliyun/db_init.json右键初始化云数据库
* 使用HBuilderx运行到浏览器

## 方法二：
* 使用[git工具](https://gitee.com/mouxangitee/unicloud-app)拉取
* 复制源码到新建的项目
* 选择项目，右键->使用命令行窗口打开所在目录，或使用系统自带的CMD进入项目运行 yarn(npm) run build 进行下载依赖包
* 创建或选择云服务空间,进入manifest.json可视化重新获取appid
* 右键上传cloudfunctions-aliyun文件夹下所有云函数(云函数上传完后请进云开发控制台将函数执行内存改至最大)
* 选择/cloudfunctions-aliyun/db_init.json右键初始化云数据库
* 使用HBuilderx运行到浏览器

以上方法都是基于[HBuilderX工具开发](https://uniapp.dcloud.io/quickstart?id=_1-%e9%80%9a%e8%bf%87-hbuilderx-%e5%8f%af%e8%a7%86%e5%8c%96%e7%95%8c%e9%9d%a2)的方法
如是想通过[vue-cli构建](https://uniapp.dcloud.io/quickstart?id=_2-%e9%80%9a%e8%bf%87vue-cli%e5%91%bd%e4%bb%a4%e8%a1%8c)请自行参考官方文档进行调试。
发布请修改运行基础路径

# 变更日志

[发行说明](https://gitee.com/mouxangitee/unicloud-app/releases) 中记录了每个发行版的详细信息更改。

# 问题

大家有问题可以在GIT的lssues提问，我会抽空帮大家解答：[提问贴](https://gitee.com/mouxangitee/unicloud-router-iview-admin/issues/I1HX8E)

# 注意
本项目使用了vue-router，所以可以不用一直在pages.json编写路由，应在“/router/router”配置。但pages.json必须配置一项存在的页面，不然会报错

# 声明
该项目为MIT协议开源项目，可商用，但出现的一切后果与作者无关，与该开源项目无关，切记！！！

如果觉得这个项目可以的话，麻烦大家动动您的小手帮我点一下上面的star，感谢

<p align="center">
  <a href='https://gitee.com/mouxangitee/unicloud-app'>
    <img src='https://gitee.com/mouxangitee/unicloud-app/widgets/widget_6.svg' alt='Fork me on Gitee'></img>
  </a>
</p>

