'use strict';
exports.main = async (event, context) => {
	let {
		data,
		db,
		tool
	} = event;
	const collection = db.collection('admin')
	return await tool.user.login({
		...data,
		queryField: ['username']
	});
}
