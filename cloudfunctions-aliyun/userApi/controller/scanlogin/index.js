'use strict';
exports.main = async (event, context) => {
	let {
		token,
		tool,
		method,
		data,
		db
	} = event;
	var collection = db.collection('code')
	var payload = await tool.user.checkToken(token)
	switch (method) {
		// 查询角色列表
		case 'get':
			var res = (await collection.doc(data.codeId).get()).data[0]
			var ndata = (new Date()).getTime()
			if (!res || res.dueDate < ndata) {
				return {
					code: -1,
					msg: '二维码不存在或已失效'
				}
			}
			return {
				code: 0,
				msg: 'success',
				data: res
			}
			break;
		case 'post':
			await collection.doc(data.codeId).update({
				uid: payload.uid
			});
			return {
				code: 0,
				msg: 'success'
			}
			break;
		case 'delete':
			await collection.doc(data.codeId).remove();
			return {
				code: 0,
				msg: 'success'
			}
			break;
		default:
			return {
				code: 404,
				msg: '请求方式错误: Request mode error'
			}
			break;
	}
}
