'use strict';
module.exports = {
	// 请求拦截器
	request: async (event) => {
		let {
			url,
			token,
			tool
		} = event
		var json = {
			code: 0,
			msg: 'ok'
		}
		if (token) {
			json = await tool.user.checkToken(token)
		}
		return json;
	},
	// 响应拦截器
	response: async (event) => {
		let {
			url,
			token,
			tool,
			data
		} = event
		if (!data.token) data.token = token
		return data
	}
}
