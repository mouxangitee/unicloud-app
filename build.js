const {
	exec
} = require("child_process");

function init() {
	exec("npm install", (error, stdout, stderr) => {
		if (error) {
			console.error(`执行install出错`);
			return;
		}
		const {
			say
		} = require("cfonts");
		const chalk = require("chalk");
		say("unicloudAPP", {
			colors: ["yellow"],
			font: "simple",
			space: true,
		});
		console.log(
			chalk.blue("┗ 安装完毕 ----------------------------") + "\n"
		);
		console.log(chalk.bold.red(" 请继续使用HBuilderx运行服务") + "\n");
		console.log(chalk.yellow(" --------------- 鸣谢 ----------------"));
		console.log(chalk.yellow("|                                     |"));
		console.log(chalk.yellow("| 开源不易，需要鼓励                  |"));
		console.log(chalk.yellow("| By: 落魄实习生（Mouxan）            |"));
		console.log(chalk.yellow("| 如有其他问题请联系:                 |"));
		console.log(chalk.yellow("|            QQ: 455171924            |"));
		console.log(chalk.yellow("|            邮箱: mouxan@163.com     |"));
		console.log(chalk.yellow("|                                     |"));
		console.log(
			chalk.yellow(" -------------------------------------") + "\n"
		);
	});
}

init();
