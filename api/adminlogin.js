import {
	post,
	get,
	deleted
} from '@/libs/axios'

// 获取扫码内容
export const getCode = data => get("scanlogin/index", data)

// 确认登录
export const loginAdmin = data => post("scanlogin/index", data)

// 取消登录
export const cancellogin = data => deleted("scanlogin/index", data)
